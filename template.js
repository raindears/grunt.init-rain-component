'use strict';

/* global __dirname */

var _grunt, _init, _;

exports.description = 'Create a RAIN component.';
exports.warnOn = '*';

exports.template = function(grunt, init, done) {

  _grunt = grunt;
  _init = init;
  _ = grunt.util._;

  var config = grunt.file.readJSON(__dirname + '/config.json');

  initPrompts();

  init.process(config, [

    init.prompt('name')
  , init.prompt('server')
  , init.prompt('view')
  , init.prompt('data')

  ], function(err, props) {

    /* jshint camelcase: false */

    props.rain_controller_name = (props.name[0].toUpperCase() + props.name.substr(1));

    writeMetaJSON(props);
    writePackageJSON(props);

    copyFiles(props);

    done();

  });

};

function initPrompts() {
  var pattern = /^(y|yes|true|n|no|false)$/i;
  var prompts = {
    'server': { 'message': 'Uses a server controller?', 'default': true }
  , 'view':   { 'message': 'Has a view?',               'default': true }
  , 'data':   { 'message': 'Uses the data layer?',      'default': true }
  };

  _.each(_.keys(prompts), function(key) {
    prompts[key] = _.defaults(prompts[key], { pattern: pattern, sanitize: sanitizeBool });
  });

  _.extend(_init.prompts, prompts);
}

function sanitizeBool(value, data, done) {
  value = (value + '').trim().toLowerCase();

  switch (value) {
  case 'y':
  case 'yes':
  case 'true':
    done(null, true);
    break;
  case 'n':
  case 'no':
  case 'false':
    done(null, false);
    break;
  case '':
    done(null, null);
  }
}

function writeMetaJSON(props) {
  var json = {
    id: props.name
  , version: props.version
  };

  if (props.view) {
    json.views = {
      index: {}
    };
  }

  writeJSON('meta.json', json);
}

function writePackageJSON(props) {
  var json = {
    name: 'rain.' + props.name
  , version: props.version
  , private: true

  , devDependencies: {
      grunt: props['grunt-version']
    }
  };

  writeJSON('package.json', json);
}

function writeJSON(path, json) {
  _grunt.file.write(path, JSON.stringify(json, null, '  ') + '\n');
}

var pathPrefix = 'rain-component/root/';

function copyFiles(props) {
  var files = expandFiles(props);

  files = files.reduce(function(files, file) {
    files[file.replace(pathPrefix, '')] = file;
    return files;
  }, {});

  _init.copyAndProcess(files, props);
}

var paths = {
  'server': [ 'server/**', '!server/data.js' ]
, 'view':     'client/**'
, 'data':     'server/data.js'
};

function expandFiles(props) {
  var files = [];

  _.each(_.keys(paths), function(path) {
    if (props[path]) {
      path = [].concat(paths[path]).map(function(p) {
        return (p[0] === '!') ? ('!' + pathPrefix + p.substr(1)) : (pathPrefix + p);
      });

      files = files.concat(_init.expand({ filter: 'isFile' }, path));
    }
  });

  files = _.pluck(files, 'rel');

  return files;
}
