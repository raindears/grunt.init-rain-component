define([], function () {
  'use strict';

  /**
   * This is the client-side controller for the 'index' view.
   *
   * @name {%= rain_controller_name %}
   * @constructor
   */
  function {%= rain_controller_name %}() {}

  {%= rain_controller_name %}.prototype = {

    /**
     * Lifecycle step 'init' is invoked right after the controller has been instantiated.
     */
    init: function () {}

    /**
     * Lifecycle step 'start' is invoked after the markup has been loaded.
     */
  , start: function () {}

    /**
     * Lifecycle step 'destroy' is invoked when the controller gets destroyed.
     */
  , destroy: function () {}

  };

  return {%= rain_controller_name %};

});
